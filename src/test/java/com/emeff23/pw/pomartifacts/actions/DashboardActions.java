package com.emeff23.pw.pomartifacts.actions;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class DashboardActions {

    private final Locator homepageText;

    public DashboardActions(Page page) {

        this.homepageText = page.locator("//*[@id=\"app\"]/div[1]/div[2]/div[2]/div/div[1]/div/div[1]/div/p");

    }

    public Locator getHomepageTextLocator() {
        return homepageText;
    }

    public String getHomepageText() {
        return homepageText.textContent();
    }

}
