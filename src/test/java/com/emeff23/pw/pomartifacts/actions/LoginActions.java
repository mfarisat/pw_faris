package com.emeff23.pw.pomartifacts.actions;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;
import com.microsoft.playwright.options.AriaRole;

import java.util.regex.Pattern;

public class LoginActions {

    private final Locator usernameField;
    private final Locator passwordField;
    private final Locator loginBtn;
    private final Locator errMsgMissingUsername;
    private final Locator errMsgInvalid;

    public LoginActions(Page page) {

        this.usernameField = page.locator("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/div[2]/input");
        this.passwordField = page.locator("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[2]/div/div[2]/input");
        /*this.loginBtn = page.getByRole(AriaRole.BUTTON,
                new Page.GetByRoleOptions().setName(
                        Pattern.compile("login", Pattern.CASE_INSENSITIVE)));

         */
        this.loginBtn = page.locator("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[3]/button");
        this.errMsgMissingUsername = page.locator("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/form/div[1]/div/span");
        this.errMsgInvalid = page.locator("//*[@id=\"app\"]/div[1]/div/div[1]/div/div[2]/div[2]/div[1]/div[1]");
        //this.errMsgMissingUsername = page.getByText("Required");
        //this.errMsgInvalid = page.getByText("Invalid credentials");

    }

    public void login(String username, String password) {

        usernameField.fill(username);
        passwordField.fill(password);
        loginBtn.click();

    }

    public Locator getErrMsgMissingUsername() {
        return errMsgMissingUsername;
    }

    public Locator getErrMsgInvalid() {
        return errMsgInvalid;
    }

}
