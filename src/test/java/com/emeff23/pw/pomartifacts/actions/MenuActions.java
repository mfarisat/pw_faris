package com.emeff23.pw.pomartifacts.actions;

import com.microsoft.playwright.Locator;
import com.microsoft.playwright.Page;

public class MenuActions {

    private final Locator mainMenuAdminBtn;

    public MenuActions(Page page) {

        this.mainMenuAdminBtn = page.locator("//*[@id=\"app\"]/div[1]/div[1]/aside/nav/div[2]/ul/li[1]/a/span");

    }

    public void navigateMenuAdmin() {

        mainMenuAdminBtn.click();

    }

}
